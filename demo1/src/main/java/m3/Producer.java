package m3;

import org.apache.rocketmq.client.producer.*;
import org.apache.rocketmq.common.message.Message;
import org.apache.rocketmq.common.message.MessageExt;
import org.apache.rocketmq.common.message.MessageQueue;

import java.util.List;
import java.util.Scanner;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

public class Producer {
    public static void main(String[] args) throws Exception{

        //创建生产者对象
        TransactionMQProducer t = new TransactionMQProducer("producer-group-3");

        //设置 name server
        t.setNamesrvAddr("192.168.64.141:9876");

        //设置监听器
        //发送“半消息”成功后，执行监听器
        t.setExecutorService(Executors.newFixedThreadPool(5));

        t.setTransactionListener(new TransactionListener() {
            @Override
            public LocalTransactionState executeLocalTransaction(Message msg, Object arg) {
                System.out.println("---------执行本地事务---------");
                System.out.println("处理业务数据："+arg);
                System.out.println("事务id："+msg.getTransactionId());
                if (Math.random()<1){
                    System.out.println("模拟事务执行情况未知");
                    return LocalTransactionState.UNKNOW;
                }
                if (Math.random()<0.5){
                    System.out.println("事务执行成功，提交事业消息");
                    //事务的执行状态需要存储
                    //事务id -> 状态
                    return LocalTransactionState.COMMIT_MESSAGE;
                }else {
                    System.out.println("事务执行失败，回滚事务消息");
                    return LocalTransactionState.ROLLBACK_MESSAGE;
                }
            }

            //执行mq服务器的事务状态回查
            @Override
            public LocalTransactionState checkLocalTransaction(MessageExt msg) {
                System.out.println("服务器正在回查消息状态");
                return LocalTransactionState.UNKNOW;
            }
        });


        //启动
        t.start();

        //
        while (true){
            System.out.print("请输入消息:");
            String s = new Scanner(System.in).nextLine();
            Message message = new Message("Topic3", s.getBytes());
            System.out.println("------发送半消息-----");
            TransactionSendResult r = t.sendMessageInTransaction(message, null);
            System.out.println("事务消息发送结果："+r.getLocalTransactionState().name());
        }
    }
}
