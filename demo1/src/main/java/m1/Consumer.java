package m1;

import org.apache.rocketmq.client.consumer.DefaultMQPushConsumer;
import org.apache.rocketmq.client.consumer.listener.ConsumeConcurrentlyContext;
import org.apache.rocketmq.client.consumer.listener.ConsumeConcurrentlyStatus;
import org.apache.rocketmq.client.consumer.listener.MessageListenerConcurrently;
import org.apache.rocketmq.client.exception.MQClientException;
import org.apache.rocketmq.common.message.MessageExt;

import java.util.List;

public class Consumer {
    public static void main(String[] args) throws MQClientException {
        //创建消费者对象
        DefaultMQPushConsumer c = new DefaultMQPushConsumer("consumer-group-1");

        //设置 name server
        c.setNamesrvAddr("192.168.64.141:9876");

        //设置接收的主题和标签(可选项)
        c.subscribe("n1", "TagA || TagB || TagC");

        //设置消息监听器
        //MessageListenerConcurrently 会启动多个线程来执行
        c.setMessageListener(new MessageListenerConcurrently() {
            @Override
            public ConsumeConcurrentlyStatus consumeMessage(List<MessageExt> msgs, ConsumeConcurrentlyContext consumeConcurrentlyContext) {
                for (MessageExt msg:msgs){
                    String s = new String(msg.getBody());
                    System.out.println("收到："+s);
                }
                //处理成功
                return ConsumeConcurrentlyStatus.CONSUME_SUCCESS;
                //处理失败,要求服务稍后重发这条消息，重新处理
//                return ConsumeConcurrentlyStatus.RECONSUME_LATER;
            }
        });

        //启动
        c.start();

    }
}
