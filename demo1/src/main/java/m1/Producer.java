package m1;

import org.apache.rocketmq.client.exception.MQBrokerException;
import org.apache.rocketmq.client.exception.MQClientException;
import org.apache.rocketmq.client.producer.DefaultMQProducer;
import org.apache.rocketmq.client.producer.SendResult;
import org.apache.rocketmq.common.message.Message;
import org.apache.rocketmq.remoting.exception.RemotingException;

import java.util.Scanner;

public class Producer {
    public static void main(String[] args) throws Exception{

    //创建生产者对象
    DefaultMQProducer p = new DefaultMQProducer("producer-gtoup-1");

    //设置 name server
    p.setNamesrvAddr("192.168.64.141:9876");

    //启动
    p.start();

    while (true){
        System.out.println("输入消息:");
        String s = new Scanner(System.in).nextLine();

        //创建消息对象
        //第一个参数：topic 相当于一级分类
        //第二个参数：tags 相当于二级分类(可选)
        Message msg = new Message("n1", "TagA", s.getBytes());

        //延时消息，设置延时级别3，延时10秒
        msg.setDelayTimeLevel(3);

         //向 Topic1 发送消息
        SendResult send = p.send(msg);
        System.out.println(send);
    }
    }
}
