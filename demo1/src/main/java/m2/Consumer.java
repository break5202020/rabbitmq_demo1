package m2;

import org.apache.rocketmq.client.consumer.DefaultMQPushConsumer;
import org.apache.rocketmq.client.consumer.listener.*;
import org.apache.rocketmq.client.exception.MQClientException;
import org.apache.rocketmq.common.message.MessageExt;

import java.util.List;

public class Consumer {
    public static void main(String[] args) throws MQClientException {
        //创建消费者对象
        DefaultMQPushConsumer c = new DefaultMQPushConsumer("consumer-group-2");

        //设置 name server
        c.setNamesrvAddr("192.168.64.141:9876");

        //设置接收的主题和标签(可选项)
        c.subscribe("Topic2", "*");

        //设置消息监听器
        c.setMessageListener(new MessageListenerOrderly() {
            @Override
            public ConsumeOrderlyStatus consumeMessage(List<MessageExt> msgs, ConsumeOrderlyContext consumeOrderlyContext) {
                for (MessageExt msg:msgs){
                    String s = new String(msg.getBody());
                    System.out.println("收到："+s);
                }
                //处理成功
                return ConsumeOrderlyStatus.SUCCESS;
                //处理失败
//                return ConsumeOrderlyStatus.SUSPEND_CURRENT_QUEUE_A_MOMENT;
            }
        });

        //启动
        c.start();

    }
}
