package m2;

import org.apache.rocketmq.client.producer.DefaultMQProducer;
import org.apache.rocketmq.client.producer.MessageQueueSelector;
import org.apache.rocketmq.client.producer.SendResult;
import org.apache.rocketmq.common.message.Message;
import org.apache.rocketmq.common.message.MessageQueue;

import java.util.List;
import java.util.Scanner;

public class Producer {
    static String[] msgs = {
            "15103111039,创建",
            "15103111065,创建",
            "15103111039,付款",
            "15103117235,创建",
            "15103111065,付款",
            "15103117235,付款",
            "15103111065,完成",
            "15103111039,推送",
            "15103117235,完成",
            "15103111039,完成"
    };

    public static void main(String[] args) throws Exception{

        //创建生产者对象
        DefaultMQProducer p = new DefaultMQProducer("producer-group-2");

        //设置 name server
        p.setNamesrvAddr("192.168.64.141:9876");

        //启动
        p.start();

        //发送消息，通过队列选择器选择固定的队列
        for (String s:msgs){
            // "15103111039,创建" -> ["15103111039,创建"]
            String[] a = s.split(",");
            Long id = Long.valueOf(a[0]);

            //封装消息
            Message msg = new Message("Topic2", s.getBytes());
            // p.send(msg,队列选择器,用来选择队列的业务数据);
            p.send(msg,new MessageQueueSelector(){
                @Override
                public MessageQueue select(List<MessageQueue> mqs, Message msg, Object arg) {
                    //第一个参数：服务器端 Topic2 中队列列表
                    Long id = (Long) arg;
                    int index = (int) (id % mqs.size());
                    return mqs.get(index);
                }
            },id);//这个id会被传递到队列选择方法中
        }
    }
}
